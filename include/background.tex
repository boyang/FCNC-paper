\section{Background Estimation}
\label{sec:background}

\subsection{Di-tau channels}
\label{sec:background:di-tau}

The region after preselections are defined as fit region (FR). For di-tau channels, the background events with real tau leptons are represented by MC samples. These include $t\bar{t}$, $t\bar{t}+H/V$ and single top events with real taus, and $Z$+jets with $Z\to\tau^{+}\tau^{-}$. The $Z\to e^{+}e^{-},\mu^{+}\mu^{-}$ processes are included for lepton faking tau background. The fake background with one or more taus faked by jets consists of the top fake (with at least one fake tau from jets in the top events), QCD and $W$+jets events. This background is modelled with the nOS region events as shown in Fig. \ref{fig:nos}, which is a data-driven estimation done separately for the 1- and 3-prong $\tauhad$'s. Compared to FR, nOS region requires leading (sub-leading) $\tauhad$ for $\tlhad$ ($\thadhad$) to have loose tau ID instead of medium and has no OS requirement, and with FR excluded. A fake model is generated from the nOS region. The MC contaminations from $Z$+jets, diboson, top events ($\ttbar$, single top, $\ttbar+H/V$) with two real taus are subtracted, with only the QCD, $W$+jets and top fake events left. The fake taus are those $\tauhad$ candidates which cannot be matched to any real taus in the MC truth.

\begin{figure}[!hbt]
\centering
\includegraphics[width=0.5\textwidth]{figures/nos.eps}
\caption{ The composition of the FR and nOS regions defined for the analysis. The y-axis denotes the leading (sub-leading) tau ID for $\tlhad$ ($\thadhad$) channel.}
\label{fig:nos}
\end{figure}


In $\tlhad$ channel, the fake tau background is represented directly by the fake model by default. Alternatively, as systematics, top fake MC in FR applied with a certain scale is added to fake model such that the fraction of top fake events matches that in the FR based on the MC prediction. For $\thadhad$ channel, the method mentioned before for $\tlhad$ systemtics is used by default. Alternatively, as systemtics, the MC prediction of the top fraction is increased by $30\%$ which is derived from an independent validation region, and the precedure is repeated. 

After the fake events are obtained in both $\tlhad$ and $\thadhad$, an overall pre-fit transfer factor is applied on the fake model to match the expected difference between the data and total non-fake background MC in the FR. This factor will be determined by the fit to data later.

To estimate the systematic uncertainty related to the nOS method, independent FR-sys and nOS-sys control regions (CR) are defined. The CRs are basically the same as the nominal regions except for lower tau IDs and with minute amount of signals. The difference between data and the data estimation of those two control regions are treated as the systematic uncertainty for this modelling method. The contamination from other FCNC signals are also studied and they turns out to be negligible.

\subsection{Lepton channels}
\label{sec:background:lepton}

For lepton channels, events with the same number of prompt leptons as the FCNC signal, which includes $t\bar{t}V$, $t\bar{t}H$, $VV$ and rare processes ($t\bar{t}WW$, $tHqb$, $tHW$, $tZ$, $tWZ$, $qqVV$, $VVV$, single top, $t\bar{t}t\bar{t}$ and $t\bar{t}t$) relies on the modeling of these processes in the MC simulations. For the background which contains at least one charge-flip electron or one fake/non-prompt lepton (later called fake lepton), instead of relying on simulation, known to poorly model fake leptons, two data-driven methods are developed to estimate them respectively.

Charge flip events are present only in $2lSS$ channel and dominantly comes from $t\bar{t}$ process, where one electron having hard bremsstrahlung plus asymmetric conversion ($e^{\pm}\to e^{\pm}\gamma*\to e^{\pm}e^{+}e^{-}$) or wrongly measured track curve while muon charge flip is negligible. Likelihood based method is developed to measure the electron charge flip rate, $\epsilon_{\text{flip}}$, from the process $Z\to e^{+}e^{-}$, as a function of electron $\pt$ and $|\eta|$ shown in Fig. \ref{fig:l_epsilon}. Sources of systematical uncertainties on $\epsilon_{\text{flip}}$ are summarized as follows: The statistical uncertainty from the likelihood method. The difference between the rates measured with the two dimensional version of likelihood method and truth-matching on simulated $Z\to e^{+}e^{-}$ events. This uncertainty is taken into account only in the ($|\eta|$, $\pt$) configurations where the ratio on these rates is not compatible with 1 within uncertainties. The variation of the rates due to variations of the $Z$-peak dilepton invariant mass region definition when the background is extracted. Event yields with charge-flip electrons are obtained by weighting (obtained from Equation \ref{eq:weight}) the events similar to FR but asking for OS lepton instead of SS.

\begin{equation}
\begin{aligned}
\text{Weight}_{ee}&=\epsilon_{\text{flip},1}*(1-\epsilon_{\text{flip},2})+\epsilon_{\text{flip},2}*(1-\epsilon_{\text{flip},1})\\
\text{Weight}_{e\mu}&=\epsilon_{\text{flip}}
\label{eq:weight}
\end{aligned}
\end{equation}
Where 1, 2 denote the indices of those two opposite-charged electrons.

\begin{figure}[!hbt]
\centering
\includegraphics[width=1\textwidth]{figures/epsilon.png}
\put(-330, 90){\textbf{(a)}}
\put(-110, 90){\textbf{(b)}}
\caption{ Electron charge flip rates computed in data with the likelihood method for anti-tight electrons (a) and tight electrons (b) using $2lSS$ channel objects. The 2015-2016 dataset has been used to estimate the rates. }
\label{fig:l_epsilon}
\end{figure}


\begin{figure}[!hbt]
\centering
\includegraphics[width=0.5\textwidth]{figures/prompteff.png}
\caption{ Electron, muon real efficiencies as measured in data. The orange band represents the systematic uncertainty from the OS Fakes normalisation uncertainty. }
\label{fig:l_prompteff}
\end{figure}


\begin{figure}[!hbt]
\centering
\includegraphics[width=1\textwidth]{figures/fakerate.png}
\put(-330, 90){\textbf{(a)}}
\put(-110, 90){\textbf{(b)}}
\caption{ Electron (a) and Muon (b) fake/non-prompt rate as measured in data. Errors correspond to the systematic uncertainty coming from the the prompt lepton background subtraction in the control region). }
\label{fig:l_fakerate}
\end{figure}

The non-prompt light lepton background is a mixture of leptons from semi-leptonic heavy-flavour decays and photon conversions. A data-driven method, called matrix method inspired from \cite{TOPQ-2010-01} is adopted. In the following study, only the two same-sign leptons are considered since opposite-charge lepton in $3l$ channel is found to be prompt 97\% of the time in Monte Carlo studies. To derive the yield of fake leptons going into FR (both leptons are tight), regions with loose leptons are used. So based on whether the two leptons pass the tight ID respectively, four regions are obtained where the event yield composites a 4-dimention vector $N_{\text{region}}^{\alpha}$ (0-componant is FR). While on the truth level, another 4-dimention vector $N_{\text{truth}}^{\beta}$ (the two leptons are real for 0-componant) is also defined based on whether the two leptons are fake respectively. If the transformation matrix $\epsilon^{\alpha\beta}$ from $N_{\text{truth}}^{\beta}$ to $N_{\text{region}}^{\alpha}$, in another word, fake rates and prompt efficiencies of leptons, is found, the only work needed is to derive $(\epsilon^{\alpha\beta})^{-1}$ and multiply with $N_{\text{region}}^{\alpha}$. Then the yield of events with fake leptons going into FR can be simply computed as a linear function of the 4 componants of $N_{\text{region}}^{\alpha}$. So in order to derive the transformation matrix, $t\bar{t}$ control region is used: $2\le N_{\text{jets}}\le 3$ and $N_{\text{b-jets}}\ge 1$; one loose light and one tight lepton with $\pt>20$ (15) GeV for $2lSS$ $(3l)$ channel; no medium $\tauhad$ candidates. 

Prompt lepton efficiencies are measured as a function of $\pt$ using data events in the $t\bar{t}$ control region as shown in Fig. \ref{fig:l_prompteff}. Two OS leptons and different flavour leptons are further required. The Tag\&Probe method is then used. To increase the purity of prompt lepton in the probes, the tag lepton is tight and trigger-matched to the lepton selected by single lepton triggers. Residual fake lepton background is subtracted using its MC prediction. In case two tag candidates are found, both leptons are considered as valid probes and used for the measurement. 

To measure the fake rate as shown in Fig. \ref{fig:l_fakerate}, SS leptons are required. The tag muon is tight and trigger-matched. For muon fake rates probing, $\mu\mu$ events are considered and if both candidates pass the tagging selection, the the leading muon is taken as the tag. Residual prompt lepton background and $t\to uH$ signal assuming that $Br(t\to uH)=0.2\%$ (the current best upper limit), are subtracted, relying on MC prediction. Electron charge flip background in electron probe sample are subtracted using data-driven estimates described before. Fake lepton rates are parametrized in two dimensions: $\pt$ and $N_{\text{bjets}}$ for electron, $\pt$ and $\Delta R(\mu, $closest jet) for muon. The different contribution from photon conversion between CRs and FR is also studied and a rescaling factor around 1.06 (1.5) for $e\mu$ ($ee$) with a relative error around 40\% is applied to fake rates.


