\section{Object reconstruction}
\label{sec:obj_reco}

In this section, various objects used in this analysis are defined, namely jets, electrons, muons, hadronically decaying taus and missing transverse energy. 

\subsection{Jets}
Jets are reconstructed using the anti-$k_t$ algorithm with a distance parameter $R=0.4$ applied to the three-dimensional topological clusters taken in the calorimeter cells. For di-tau (lepton) channels, only jets with $\pt>30 (25)$~GeV and $|\eta|<4.5 (2.5)$ are considered. To suppress jets produced in additional pile-up interactions, jets with $\pt<60$~GeV and $|\eta|<2.4$ are required to have a Jet Vertex Tagger (JVT \cite{JVT}) cut. The JVT is the output of the jet vertex tagger algorithm used to identify and select jets originating from the hard-scatter interaction through the use of tracking and vertexing information. 

For di-tau channels only, the forward region pile-up jets are supressed by requiring that jets with $\pt < 50$ GeV and $|\eta|$>2.5 pass the selection of fJVT>0.4 \cite{fJVT}\footnote{Neither JVT nor fJVT is applied to the tracking edge region 2.4<$|\eta|$<2.5.}. About $10\%$ of selected jets in the signal are in the forward detector region. A ``jet cleaning'' cut is applied on all the jets with $\pt>20$ GeV, and the events with jets not passing this cut are discarded.

\subsection{b-tagging}
The {\tt MV2c10} \cite{btag1} algorithm is used to identify the jets initiated by $b$-quarks. A working point corresponding to an average efficiency of 70\% for jets containing $b$-quarks is chosen. For $c$- and light-jets, the rejection factors are about 12 and 381, respectively \cite{btag2}.

\subsection{light leptons}

Electron candidates are identified by tracks reconstructed in the inner detector and the matched cluster of energy deposited in the electromagnetic calorimeter. In di-tau (lepton) channels, the lowest $\pt$ requirement for electron candidates is 15 (10)~GeV and $|\eta|<2.47$. The transition region, $1.37<|\eta|<1.52$, between the barrel and end-cap calorimeters is excluded. A multivariate likelihood discriminant combining shower shape and track information is used to distinguish real electrons from hadronic showers (fake electrons). For di-tau channels, Medium likelihood-based identification point and a Gradient isolation working point are used. For lepton channels, loose electron working point is used for preselection, and no isolation is required. To further reduce the non-prompt electron contribution, the track is required to be consistent with originating from the primary vertex; requirements are imposed on the transverse impact parameter significance ($|d_0|/\sigma_{d_0}$)and the longitudinal impact parameter ($|\Delta z_0\sin\theta_l|$). 

Muon reconstruction begins with tracks reconstructed in the MS and is matched to tracks reconstructed in the inner detector. Muon candidates are required to have $\pt>10$~GeV and $|\eta|<2.5$. For di-tau channels, a \texttt{Medium} identification selection based on the requirements on the number of hits in the ID and the MS is satisfied. A \texttt{Gradient} isolation criterion is also required. For lepton channels, the transverse impact parameter requirement for muon candidates is sightly tighter than for electrons, while the longitudinal impact parameter selection is the same. No isolation is required in the object preselection.

In di-tau channels, the trigger matching between the offline and trigger level lepton objects is also required for the corresponding leptons selected for the analysis. The minimum offline lepton $p_{\text{T}}$ should be 1 GeV above the trigger threshold. For the $\tmhad$ channel in 2016, the offline muon $p_{\text{T}}$ is 2 GeV above the trigger threshold.

For lepton channels, a lepton MVA called {\tt PromptLeptonIso} has been developed to better reject non-prompt light leptons coming from heavy flavor. It improves over standard cut based selections based upon impact parameter, isolation and lepton identification. The main idea is to identify non-prompt light leptons using lifetime information associated with a track jet that matches the selected light lepton. This lifetime information is computed using tracks contained within the jet. {\tt PromptLeptonIso} also includes information related to the isolation of the lepton to reject non-prompt leptons. Distribution of {\tt PromptLeptonIso} is shown in Fig. \ref{fig:l_PromptLeptonIso}. The optimised working point is {\tt PromptLeptonIso} $<-0.50$ which is measured using the tag and probe method with $Z\to l^{+}l^{-}$events. For both electrons and muons, the dominant systematics is coming from pile-up dependence. Overall the systematics are at maximum of 3\% at low $\pt$ and decreasing at a function of $\pt$.Another MVA has also been developed to reduce electron charge flips by a factor 17 for a 95\% electron efficiency.

\begin{figure}[!hbt]
\centering
\includegraphics[width=1\textwidth]{figures/prompt.png}
\put(-330, 90){\textbf{(a)}}
\put(-110, 90){\textbf{(b)}}
\caption{ Distributions of {\tt PromptLeptonIso} for prompt and non-prompt (a) muons and (b) electrons with a log scale. The non-prompt distribution is scaled to the number of prompt leptons. The lepton MVA is trained in such a way that leptons which are most likely prompts have a negative score (highest prompt score is -1), while leptons which are most likely non-prompts have a positive score (highest non-prompt score is +1). }
\label{fig:l_PromptLeptonIso}
\end{figure}

\subsection{Hadronic tau decays}
The hadronic tau candidates are seeded by jets reconstructed by the anti-$k_t$ algorithm, which is applied on calibrated topo clusters with a distance parameter of R=0.4. They are required to have $\pt>25$~GeV and $|\eta|<2.5$. The transition region between the barrel and end-cap calorimeters ($1.37<|\eta|<1.52$) is excluded. An identification algorithm based on Boosted Decision Trees is applied to discriminate the visible decay products of hadronically decaying tau lepton $\tauhad$ from jets initiated by quarks or gluons. Different BDT working points are provided and required at different levels depending on the analysis channel. In the nominal event selection, the hadronic tau candidates are required to have one or three charged tracks and an absolute charge of one, and pass the \texttt{Medium} tau ID to reject the jets. A dedicated BDT variable is also used to veto the taus which are actually electrons. If the $\tauhad$ candidate is also tagged as a $b$-jet, then this tau object is also not used.

For di-tau channels, they are futher required to not overlap with a very loose electron candidate. The fake $\tauhad$ background in the analysis is estimated by reverting the $\tauhad$ ID. 

For lepton channels, the candidates that overlap with low-$\pt$ reconstructed muons are removed. The primary vertex matched to the tracks of the $\tauhad$ candidate is required to be the primary vertex of the event, in order to reject fake candidates arising from pileup collisions.


\subsection{Missing transverse energy}
The missing transverse massing $\met$ is computed using the fully calibrated and reconstructed physics objects as described above. The TrackSoftTerm (TST) algorithm is used to compute the SoftTerm of the $\met$. 

\subsection{Overlap removal}
When two objects are close geometrically with $\Delta R$ less than a certain threshold, one object that is kept is following this order: muons, electron, taus and jets. This is the general strategy recommended for overlap removal. 

For di-tau channels, the threshold is always kept at 0.2. Muons for the overlap removal with $\tauhad$ are required to have $\pt>2$~GeV, which means a tighter cut than those defined above. 

For lepton channels, the threshold is 0.1 (0.2/0.3) between electrons and muons (jets and electrons or muons/taus and light leptons). In addition, any electron candidate within $\Delta R=0.1$ of another electron candidate with higher $\pt$ is removed. if a muon candidate and a jet lie within $\Delta R=\text{min}(0.4,0.04+10[\GeV]/\pt(\mu))$ of each other, the muon is removed.

\section{Event selection}
\label{sec:reconstruction}

\subsection{Di-tau channels}

The final state of the di-tau signal is characterized by two taus (from the Higgs) and a quark jet ($c$ or $u$) coming from one top (or anti-top) quark decay, and three other jets from the decay of the other top ($t\to Wb\to jjb$). So in each event, if all jets from the top hadronic decay and the jet from $t\to qH$, denoted as the FCNC jet, pass the jet selection, there should be at least four jets. However, some jets have $\pt$ less than 30 GeV and may fail to pass the jet selection. Therefore, at least three reconstructed jets are required, with exactly one jet tagged as a $b$-jet. Events must contain at least one primary vertex. In the $\tlhad$ ($\thadhad$) channel, exactly one (no) light lepton and at least one (two) medium $\tauhad$(s) candidates are required. Opposite sign (OS) is also required.

For the missing jet in three-jet events, it is most likely the subleading jet from $W$ decay. These 3-jet events are still kept if the FCNC jet can be found and matched with the Higgs to reconstruct the top. It is done as follows. In the 3-jet events, if the three jets, denoted by $j_1$, $j_2$, $b$, satisfy
\begin{equation}
\chi_{Wb}^2 \equiv \left(\frac{m_{j_1 j_2}-80.4}{20}\right)^2 + \left(\frac{m_{j_1 j_2 b}-172.5}{25}\right)^2 <5,
\label{eq:eq4}
\end{equation}
where the mass is in GeV, the event is discarded. In these events, a good hadronic top is reconstructed, but the FCNC jet from the other top is missing.

If Eq.~(\ref{eq:eq4}) is not satisfied, the FCNC jet is identified by the least sum of angular distances, $\Delta R_{3j}$, expressed as 
\begin{equation}
\Delta R_{3j} \equiv \Delta R(j_c ,H) + \Delta R(j_W ,b) \: ,
\label{eq:eq5}
\end{equation}
where the $j_W$ denotes the jet from the $W$ decay, and the event is kept.

For the events with at least four jets (denoted as 4-jet events), the three leading ones other than the $b$-jet are considered. Out of the three possible combinations that form the two sets of top decay products, the one with the least sum of angular distances, $\Delta R_{4j}$, is chosen, which is defined as
\begin{equation}
\Delta R_{4j} \equiv \Delta R(j_c ,H) + \Delta R(j_{1} ,b) + \Delta R(j_{2} ,b) + \Delta R(j_{1} ,j_{2}),
\label{eq:eq6}
\end{equation}
where $j_{1,2}$ are the jets from the $W$ decay. No explicit $c$-tagging is used, and as can be seen above, the FCNC jet is found through pure kinematic criteria such as $\Delta R$.

%To improve the rate of finding the FCNC jet correctly, as explained in \cite{fcnc_PRD}, the helicity angle of the Higgs, $\theta_H$, is studied. For the decay $t\to qH$, $\theta_H$ is defined as the angle between the momentum of the top quark in the laboratory frame and the momentum of the Higgs in the center-of-mass frame of the top quark. If the Higgs helicity angle satisfies $\cos\theta_H<-0.8$ or $\cos\theta_H>0.5$ for the events with $m_{bj_1}>170$ GeV ($m_{bj_1}$ is the invariant mass of the $b$-jet and the leading jet presumably from the $W$ decay), the $b$-jet candidate and the FCNC jet candidate are exchanged.

The method introduced in \cite{fcnc_PRD} is used to recontruct the ditau mass and momentum by taking the $\tau$ decay kinematics into account. To determine the 4-momenta of the invisible decay products of the tau decays, the following $\chi^2$ in Eq.~\ref{eq:eq2}, based on the probability functions above and the constraints from the tau mass, the Higgs mass and the measured $\met$, is defined,
\begin{eqnarray}
\begin{array}{ll}
\chi^2 = & -2\ln \mathcal{P}_1 -2\ln \mathcal{P}_2 + \left( \frac{m_{\tau_1}^{\text{fit}} - 1.78}{\sigma_{\tau}} \right)^2 +  \left( \frac{m_{\tau_2}^{\text{fit}} - 1.78}{\sigma_{\tau}} \right)^2 +  \left( \frac{m_{H}^{\text{fit}} - 125}{\sigma_{\text{Higgs}}} \right)^2 + \\
 & \left( \frac{E_{x,\text{miss}}^{\text{fit}} - E_{x,\text{miss}}}{\sigma_{\text{miss}}} \right)^2 + \left( \frac{E_{y,\text{miss}}^{\text{fit}} - E_{y,\text{miss}}}{\sigma_{\text{miss}}} \right)^2 ,
\end{array}
\label{eq:eq2}
\end{eqnarray}

\begin{figure}[!hbt]
\centering
\includegraphics[width=0.4\textwidth]{figures/lephad/lh_3j_chi2_os.eps}
\put(-50, 80){\textbf{(a)}}
\put(-57, 95){\footnotesize{$lh$ 3-jet}}
\includegraphics[width=0.4\textwidth]{figures/lephad/lh_4j_chi2_os.eps}
\put(-50, 80){\textbf{(b)}}
\put(-57, 95){\footnotesize{$lh$ 4-jet}}
\caption{ The distributions of $\chi^2$ in Eq. \ref{eq:eq2} in the 3-jet (a) and 4-jet (b) categories in the $\tlhad$ channel. }
\label{fig:lephad_chi2}
\end{figure}

where $\mathcal{P}_i(\Delta R)$ are the probability distribution of the angular distance of the visible and invisible decay products in the tau decay, parametrized as a function of the momentum of the tau lepton. $m_{\tau_{1,2}}^{\text{fit}}$,  $m_{H}^{\text{fit}}$ and $E_{xy,\text{miss}}^{\text{fit}}$ are the calculated tau mass, Higgs mass, and missing transverse energy with the scanned parameters. In the $\taulep$ mode where two neutrinos are present, it is extended to be the joint probability distribution of $\Delta R$ and $m_{\text{mis}}$ with $m_{\text{mis}}$ being the invariant mass of the neutrinos, denoted by $\mathcal{P}(\Delta R, m_{\text{mis}})$. These probability density functions are obtained from the MC simulation. The free parameters scanned are the 4-momentum components of the invisible decay products for each tau decay. In the $\tauhad$ mode, only three momentum components are scanned since a single neutrino is massless.The corresponding mass resolutions, $\sigma_{\tau}$ and $\sigma_{\text{Higgs}}$, are set to 1.8~GeV and 20~GeV respectively. The invisible 4-momenta are obtained by minimizing the combined $\chi^2$ for each event\footnote{
The coarse global minimum of the $\chi^2$ in Eq. \ref{eq:eq2} is first obtained by scanning the ($\eta$, $\phi$) of the netrino(s) from one tau, and repeating for the other tau. Then a final minimum is obtained around it with the MINUIT packge \cite{MINUIT}.
}. Figure \ref{fig:lephad_chi2} shows the distributions of $\chi^2$ in the $\tlhad$ channel. Good agreement between data and background predictions are achieved.

After the momentum reconstruction of neutrinos, a cut is further applied on the visible energy ratio of the $\tau$ candidates. For $\taulep$ ($\tauhad$) candidates, the ratio is required to be less than 0.95 (0.99).


\subsection{Lepton channels}

In the case of the $2l$SS channel, two light leptons ($l_0$ and $l_1$) are required to have the same charge and to pass the tight lepton requirements with $\pt$>20 GeV. Events must have at least 4 jets, of which 1 or 2 must be b-tagged.

In the $3l$ channel, the total charge of the three leptons in these events must be $\pm$1. The lepton of opposite charge to that of the other two is designated $l_0$. Of the two remaining leptons, the one with smallest $\Delta R(l,l_0)$ is designated $l_1$ and the other is $l_2$. Leptons $l_1$ and $l_2$ are required to pass the tight selections with $\pt$>15 GeV. Lepton $l_0$ is rarely fake or non-prompt, therefore only Loose isolation is required on top of the loose lepton definition. The latter also ensures that no electrons and muons are overlapping at small $\Delta R$. To remove leptons from quarkonium decays, all same-flavor $l^{+}l^{-}$ pairs must satisfy m($l^{+}l^{-}$)>12 GeV. To remove potential backgrounds with $Z$ decays to $ll\gamma^{(*)}$ → $lll'(l')$, where one lepton has very low momentum and is not reconstructed, the three-lepton invariant mass must satisfy $|m(3l)-91.2$GeV$|>10$GeV. There must be at least 2 jets, of which at least 1 must be b-tagged. An additional cut to reject the $\bar{t}tZ$ background is used in the training of the $3l$ analysis, where all same-flavour $l^{+}l^{-}$ pairs in the event mustsatisfy $|m(l^{+}l^{-})-91.2\text{GeV}|>10$GeV, referred to as the $Z$ veto.

