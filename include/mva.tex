\section{MVA}
\label{sec:mva}

In this section, we investigate the sensitivity of probing BR$(t\to qH)$ using one of the Multi-Variate Analysis (MVA) methods, the Gradient Boosted Decision Trees (GBDT) method~\cite{BDT,BDT2}, with the {\tt TMVA} software package. The BDT output score is in the range between -1 and 1. The most signal-like events have scores near 1 while the most background-like events have scores near -1. The signal and background samples are randomly divided into two equal parts based on event number. The BDT is trained with one part, while tested on the other part and vise versa.

\subsection{Di-tau channels}

The signal purity is different for different decay modes ($\tlhad$ and $\thadhad$) and for different event topologies (3-jet and 4-jet events), while $t\to cH$ and $t\to uH$ channels are merged together. To maximize the overall sensitivity, the signal region is thus divided into 4 categories (3-jet/4-jet $\tlhad$/$\thadhad$). A number of variables as the BDT inputs listed below are used to train and test events in each signal region for maximal signal acceptance and background rejection. The Gradient BDT parameters (such as number of cuts and number of trees) are also tuned to optimize the sensitivity and avoid overtraining. The BDTG output in FR is shown in Figure \ref{fig:os_bdt}.

\begin{figure}[!hbt]
\centering
\includegraphics[width=0.45\textwidth]{figures/lephad/lh_3j_bdt_os.eps}
\put(-100, 170){\textbf{(a)}}
\put(-70, 105){\small{$lh$ 3-jet}}
\includegraphics[width=0.45\textwidth]{figures/lephad/lh_4j_bdt_os.eps}
\put(-100, 170){\textbf{(b)}}
\put(-70, 105){\small{$lh$ 4-jet}}\\
\includegraphics[width=0.45\textwidth]{figures/hadhad/hh_3j_bdt_os.eps}
\put(-100, 170){\textbf{(c)}}
\put(-70, 100){\small{$hh$ 3-jet}}
\includegraphics[width=0.45\textwidth]{figures/hadhad/hh_4j_bdt_os.eps}
\put(-100, 170){\textbf{(d)}}
\put(-70, 100){\small{$hh$ 4-jet}}\\
\caption{ The BDT output distributions in the OS FRs of $\tlhad$ 3-jet (a), $\tlhad$ 4-jet (b), $\thadhad$ 3-jet (c) and $\thadhad$ 4-jet (d). The expected signal (scaled by the indicated factors) is also shown. The errors are statistical only. }
\label{fig:os_bdt}
\end{figure}

\begin{enumerate}
\item The invariant mass of reconstructed FCNC $t$ as well as $H$.
\item The transverse momenta of the $\tauhad$('s).
\item A measure of how central the $\met$ lies between the two taus in the transverse plane, named $\met$ centrality defined as
\begin{eqnarray}
\begin{array}{l}
\met~\text{centrality} = {(x+y)}/{\sqrt{x^2+y^2}}, \\
\text{with}~x=\frac{\sin(\phi_{\text{miss}}-\phi_{\tau_1})}{\sin(\phi_{\tau_2}-\phi_{\tau_1})},  y=\frac{\sin(\phi_{\tau_2}-\phi_{\text{miss}})}{\sin(\phi_{\tau_2}-\phi_{\tau_1})} ,
\end{array}
\label{eq:eq3}
\end{eqnarray}
\item The momentum fraction carried by the visible decay products from the tau mother. It is based on the best-fit 4-momentum of the reconstructed neutrino(s).
\item The invariant mass of the $b$-jet and the two jets from the $W$ decay, and reflects the top mass in the decay $t\to Wb \to j_1j_2b$. (For 4-jet events only).
\item The invariant mass of the $b$-jet and the leading jet presumably from the $W$ decay. 
\item The transverse mass calculated from the lepton and $\met$ in the $\tlhad$ channel, named $m_{\text{T,lep}}$ defined as
\begin{equation}
m_{\text{T,lep}} = \sqrt{2 p_{\text{T,lep}} E_{\text{T}}^{\text{miss}} \left(1-\cos\Delta\phi_{\text{lep,miss}} \right)}.  
\end{equation}
\end{enumerate}
For $\tlhad$ only:
\begin{enumerate}
\item The invariant mass of the lepton and the jet which has the smallest angular distance with the tau candidate.
\item The invariant mass of the tau and the jet which has the smallest angular distance with the tau candidate.
\item The difference between missing transverse energy $E_{T}^{\text{miss}}$ and the (sum of) transverse momentum of neutrino(s) perpendicular to $E_{\text{T}}^{\text{miss}}$ (for $\tlhad$ only) and in the direction of $E_{\text{T}}^{\text{miss}}$.
\end{enumerate}

\subsection{Lepton channels}

For lepton channels, the approach is very similar to that followed by the $t\bar{t}H$ multilepton analysis \cite{HIGG-2013-26}, with the same pre-MVA region and a subset discriminating variables. To reject events with fake/non-prompt leptons and charged flip electrons as well as $t\bar{t}W$ and $t\bar{t}Z$, BDT training is processed separately for simulated MC events and data-driven estimated events. The final BDT score is a linear combination of those two scores normalized to 1. The weights of those two BDT scores are determined by maximizing the sensitivity while the binning strategy is tuned at the same time. For $2lSS$ channel, signal samples are merged to gain statistics. For $3l$ channels, siginificant difference are observed and the training is done separately for $t\to cH$ and $t\to uH$. The input variables are listed below and the BDTG output in FR is shown in Figure \ref{fig:ll_bdt}.

\begin{figure}[!hbt]
\centering
\includegraphics[width=1\textwidth]{figures/ll_bdt.png}
\caption{The pre-fit BDT distribution observed in data (points with error bars) and expected (histograms) in the $2lSS$ (left), $3l$ (right) signal regions. $Br(t\to uH)(\text{up})=Br(t\to cH)(\text{down})=0.2\%$ is assumed. The background contributions are shown as filled histograms. The top FCNC signal is shown as a filled histogram on top of the backgrounds.}
\label{fig:ll_bdt}
\end{figure}

\begin{enumerate}
\item The three (two) leading lepton $\pt$.
\item Dilepton invariant mass (all combinations).
\item Number of jets and b-tagged jets.
\item The angular distance between $l_1$ and closest jet
\item Missing transverse energy plus $H_\text{T}$
\end{enumerate}

For $2lSS$ only:
\begin{enumerate}
\item Maximum $|\eta|$ between two leptons
\item Missing transverse energy
\end{enumerate}

For $3l$ only:
\begin{enumerate}
\item Two leading jets' $\pt$ and $b$-tagged jet $\pt$
\item Missing transverse energy
\item The angular distance between $l_0$ and $l_1$ ($l_2$/closest $b$-tagged jet).
\end{enumerate}
